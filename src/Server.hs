{-# LANGUAGE OverloadedStrings #-}

import Protocol

import Control.Concurrent (threadDelay)
import Control.Lens 
import Data.Aeson (encode)
import Network.WebSockets (PendingConnection, acceptRequest, sendTextData, 
                           toLazyByteString, runServer, Connection)

main :: IO ()
main = do
    putStrLn "running server..."
    runServer "0.0.0.0" 3000 serverApp

serverApp :: PendingConnection -> IO ()
serverApp pc = do
    conn <- acceptRequest pc
    putStrLn "new client"
    handleApp newProtocol conn

handleApp :: Protocol -> Connection -> IO ()
handleApp protocol conn = do
    threadDelay 1000000
    sendTextData conn (encode protocol) 
    handleApp (value +~ 1 $ protocol) conn

