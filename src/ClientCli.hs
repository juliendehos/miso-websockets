import Protocol

import Control.Monad (forever)
import Data.Aeson (decode)
import Network.Socket (withSocketsDo)
import Network.WebSockets (runClient, ClientApp, fromLazyByteString, receiveData) 

main :: IO ()
main = withSocketsDo $ runClient "127.0.0.1" 3000 "/" myApp

myApp :: ClientApp ()
myApp conn = forever $ do
    m <- decode <$> receiveData conn
    print (m :: Maybe Protocol)

