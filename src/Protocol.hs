{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module Protocol where

import Data.Aeson (FromJSON, ToJSON)
import Control.Lens 
import GHC.Generics (Generic)

newtype Protocol = Protocol { _value :: Int }
    deriving (Eq, Generic, Show)

instance ToJSON Protocol
instance FromJSON Protocol

makeLenses ''Protocol

newProtocol :: Protocol
newProtocol = Protocol 0

