{-# LANGUAGE OverloadedStrings #-}

import Miso
import Miso.String (ms, MisoString)
import Protocol

newtype Model = Model
    { _modelText :: MisoString
    } deriving (Eq)

data Action
    = ActionNone
    | ActionWebSocket (WebSocket Protocol)

main :: IO ()
main = startApp App
    { initialAction = ActionNone
    , update        = updateModel
    , view          = viewModel
    , model         = Model "not connected"
    , subs          = [ websocketSub uri protocols ActionWebSocket ]
    , events        = defaultEvents
    , mountPoint    = Nothing
    }
    where protocols = Protocols []
          uri       = URL "ws://127.0.0.1:3000"

updateModel :: Action -> Model -> Effect Action Model
updateModel ActionNone m = noEff m
updateModel (ActionWebSocket (WebSocketMessage msg)) m
    = noEff m { _modelText = ms $ show $ _value msg }
updateModel (ActionWebSocket (WebSocketError err)) m
    = noEff m { _modelText = ms (show err) }
updateModel (ActionWebSocket WebSocketClose {}) m
    = noEff m { _modelText = "closed" }
updateModel (ActionWebSocket WebSocketOpen) m
    = noEff m { _modelText = "opened" } 

viewModel :: Model -> View Action
viewModel m = div_ []
    [ h1_ [] [ text "miso-websockets" ]
    , p_ [] [ text ("text: " <> _modelText m)]
    ]

