server:
	nix-shell -A ghc --run "cabal --builddir=dist-ghc --config-file=config/ghc.config build server"

client-web:
	nix-shell -A ghcjs --run "cabal --builddir=dist-ghcjs --config-file=config/ghcjs.config build client-web"
	ln -sf ../`find dist-ghcjs/ -name all.js` public/

client-cli:
	nix-shell -A ghc --run "cabal --builddir=dist-ghc --config-file=config/ghc.config build client-cli"

run-server:
	nix-shell -A ghc --run "cabal --builddir=dist-ghc --config-file=config/ghc.config run server"

run-client-web:
	firefox public/index.html

run-client-cli:
	nix-shell -A ghc --run "cabal --builddir=dist-ghc --config-file=config/ghc.config run client-cli"

clean:
	rm -rf dist-* public/all.js 

