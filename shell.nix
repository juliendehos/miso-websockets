let pkgs = import ./nixpkgs.nix ;
in {
  ghc = (pkgs.haskell.packages.ghc.callCabal2nix "app" ./. {}).env;
  ghcjs = (pkgs.haskell.packages.ghcjs.callCabal2nix "app" ./. {}).env;
}

